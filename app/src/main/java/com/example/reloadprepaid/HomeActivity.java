package com.example.reloadprepaid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.reloadprepaid.activity.LoginActivity;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.fragment_activity.FragmentFavourite;
import com.example.reloadprepaid.fragment_activity.FragmentHistory;
import com.example.reloadprepaid.fragment_activity.FragmentPrepaidReload;
import com.example.reloadprepaid.fragment_activity.FragmentRetellerPrice;
import com.example.reloadprepaid.fragment_activity.FragmentSetting;
import com.example.reloadprepaid.fragment_activity.FragmentSummary;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    AllAccess allAccess;
    FragmentManager fragmentManager;
    NavigationView navigationView;
    View headerView;
    TextView tvDrawerName, tvDrawerEmail, tvDrawerMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        allAccess = (AllAccess) getApplication();

        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Home");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);// set default item highlighted

        initializeComponents();
        readAPI();
        LocalBroadcastManager.getInstance(HomeActivity.this).registerReceiver(broadcastReceiver, new IntentFilter("update_profile"));

        fragmentManager.beginTransaction().replace(R.id.fragment_content, new FragmentSummary()).commit();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            readAPI();
        }
    };

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(HomeActivity.this).unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    private void initializeComponents() {
        fragmentManager = getSupportFragmentManager();
        allAccess = (AllAccess) getApplication();

        //set/inflate header manually to change textview content in header, also remove header from NavigationView in activity_home
        headerView = navigationView.inflateHeaderView(R.layout.nav_header_home);
        //use headerview to identify textview id in well use guess it, the headerview
        tvDrawerName = (TextView) headerView.findViewById(R.id.tvDrawerName);
        tvDrawerEmail = (TextView) headerView.findViewById(R.id.tvDrawerEmail);
    }

    private void readAPI() {
        Log.e("readapi", "reading1");
        APIClient client = ServiceGenerator.createService(APIClient.class);
        final Call<ResponseBody> result = client.getDashboard(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()) {
                        Log.e("readapi", "reading2");
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        tvDrawerName.setText(jsonObject.getString("name"));
                        tvDrawerEmail.setText(jsonObject.getString("email"));
                    } else {
                        Log.e("readapi", "reading3");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {

                Log.e("readapi", "reading4");
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            setTitle("Home");
            fragmentManager.beginTransaction().replace(R.id.fragment_content, new FragmentSummary()).commit();
        } else if (id == R.id.nav_prepaidreload) {
            setTitle("Prepaid Reload");
            fragmentManager.beginTransaction().replace(R.id.fragment_content, new FragmentPrepaidReload()).commit();
        } else if (id == R.id.nav_favourite) {
            setTitle("Favourite");
            fragmentManager.beginTransaction().replace(R.id.fragment_content, new FragmentFavourite()).commit();
        } else if (id == R.id.nav_history) {
            setTitle("History");
            fragmentManager.beginTransaction().replace(R.id.fragment_content, new FragmentHistory()).commit();
        } else if (id == R.id.nav_retaillerprice) {
            setTitle("Reseller Price");
            fragmentManager.beginTransaction().replace(R.id.fragment_content, new FragmentRetellerPrice()).commit();
        } else if (id == R.id.nav_setting) {
            setTitle("Settings");
            fragmentManager.beginTransaction().replace(R.id.fragment_content, new FragmentSetting()).commit();

        } else if (id == R.id.nav_logout) {
            AlertDialog alertDialog = new AlertDialog.Builder(HomeActivity.this).create();
            alertDialog.setTitle("Logout");
            alertDialog.setMessage("Are you sure you want to log out?");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });
            alertDialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}

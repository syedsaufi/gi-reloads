package com.example.reloadprepaid.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    EditText etUsername, etPassword, etVerify, etName, etEmail, etMobile;
    String username, password, verify, name, email, mobile;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initializeComponents();
    }

    private void initializeComponents() {
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etVerify = (EditText) findViewById(R.id.etVerify);
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etMobile = (EditText) findViewById(R.id.etMobile);
    }

    public void showDialog(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(RegisterActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        alertDialog.show();
    }

    public boolean verification() {
        username = etUsername.getText().toString();
        password = etPassword.getText().toString();
        verify = etVerify.getText().toString();
        name = etName.getText().toString();
        email = etEmail.getText().toString();
        mobile = etMobile.getText().toString();

        if (username.equals("") || password.equals("") || verify.equals("") || name.equals("") || email.equals("") || mobile.equals("")) {
            showDialog("Empty field", "You need to fill in all the field to complete your registration");
            return false;
        }
        if (!(verify.equals(password))) {
            showDialog("Wrong password", "Your password verification should be the same as the password you have just filled.");
            return false;
        }

        return true;
    }

    public void callAPI() {
        String data = "{\"username\": \"" + username + "\", \"password\": \"" + password + "\", \"name\": \"" + name + "\", \"email\": \"" + email + "\", \"mobile\": \"" + mobile + "\"}";
        Log.e("data", data);
        progressDialog = ProgressDialog.show(RegisterActivity.this, "Loading", "Creating account...");
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Call<ResponseBody> result = client.getRegister("email", data);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                if (response.isSuccess()) {
                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        AlertDialog alertDialog = new AlertDialog.Builder(RegisterActivity.this).create();
                        alertDialog.setTitle("Registration successfull");
                        alertDialog.setMessage("Your registration is successful. An email have been sent to your email for activation.");
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                finish();
                            }
                        });
                        alertDialog.show();
                    }

                } else {
                    progressDialog.dismiss();

                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.errorBody().string()));
                        if (jsonObject.getString("code").equals("401")) {
                            Log.e("Something happen here", "alksdasd");
                            showDialog("Email already exist", "The email you have entered already exist, please register with a different email.");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                showDialog("Error", "Something is not right, please make sure you have a good internet connection and try again later.");
            }
        });
    }

    public void onClickRegisterAccount(View view) {
        if (verification()) {
            callAPI();
        }
    }

}

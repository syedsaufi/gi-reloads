package com.example.reloadprepaid.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    EditText etEmail;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initializeComponents();
    }

    private void initializeComponents() {
        etEmail = (EditText) findViewById(R.id.etEmail);
    }

    public void onClickEmail(View view) {
        Log.e("forgotPassword", "test1");
        if (verification()) {
            progressDialog = ProgressDialog.show(ForgotPasswordActivity.this, "Loading,", "Please wait..", true);
            APIClient client = ServiceGenerator.createService(APIClient.class);
            Call<ResponseBody> result = client.getForgpotPassword(etEmail.getText().toString());
            result.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response) {
                    if (response.isSuccess()) {
                        progressDialog.dismiss();
                        showAlert("Success", "Your password have been reset. Please check your email for the new password.");
                    } else {
                        progressDialog.dismiss();
                        showAlert("Not Successful", "Email fail to send, please make sure you insert the correct email and try again.");
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }
    }

    public boolean verification() {
        if (etEmail.getText().toString().equals("")) {
            showAlert("Error", "Please insert your email to reset your password.");
            return false;
        }
        return true;
    }

    public void showAlert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        alertDialog.show();
    }
}

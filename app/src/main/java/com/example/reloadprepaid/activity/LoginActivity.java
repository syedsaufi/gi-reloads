package com.example.reloadprepaid.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.reloadprepaid.HomeActivity;
import com.example.reloadprepaid.R;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    AllAccess allAccess;
    EditText etUsername, etPassword;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initializeComponents();
    }

    private void initializeComponents() {
        allAccess = (AllAccess) getApplication();
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    public boolean verification(){
        if (etUsername.getText().toString().equals("") || etPassword.getText().toString().equals("")){
            showAlert("Error", "Please insert name username and password.");
            return false;
        }
        return true;
    }

    public void onClickLogin(View view) {
        if (verification()){
            progressDialog = ProgressDialog.show(LoginActivity.this, null, "Authenticating .....", true);
            APIClient client = ServiceGenerator.createService(APIClient.class);
            Call<ResponseBody> result = client.getLogin(etUsername.getText().toString(), etPassword.getText().toString());
            result.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response) {
                    if (response.isSuccess()) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.body().string()));
//                        JSONObject resultObject = jsonObject.getJSONObject("result");
//                        Log.e("auth_key", resultObject.getString("auth_key"));
                            allAccess.setAuth_key(jsonObject.getString("auth_key"));

                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            progressDialog.dismiss();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (response.code() == 401) {
                            progressDialog.dismiss();
                            showAlert("Error", "Wrong username or password.");
                        } else {
                            progressDialog.dismiss();
                            showAlert("Not activated", "You need to activate your account first via your email.");
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    progressDialog.dismiss();
                    showAlert("Error", "Something is not right, please make sure you have a good internet connection and try again later.");

                }
            });
        }
    }

    public void onClickRegister(View view) {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    public void onClickForgotPassword(View view) {
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_forgot_password, null);
        final EditText etxtEmail = (EditText) dialogView.findViewById(R.id.etxtEmail);

        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setView(dialogView)
                .setTitle("Reset Your Password")
                .setPositiveButton("Reset", null)
                .setNegativeButton("Cancel", null)
                .create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (etxtEmail.getText().toString().trim().equals("")) {
                            showAlert("Error", "Please enter title");
                        } else {
                            progressDialog = ProgressDialog.show(LoginActivity.this, null, "Please wait..", true);
                            APIClient client = ServiceGenerator.createService(APIClient.class);
                            Call<ResponseBody> result = client.getForgpotPassword(etxtEmail.getText().toString());
                            result.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Response<ResponseBody> response) {
                                    progressDialog.dismiss();
                                    if (response.isSuccess()) {
                                        alertDialog.dismiss();
                                        showAlert("Success", "Please check your email");
                                    } else {
                                        showAlert("Failed", "Please try again");
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    showAlert("Network error", "Please try again");
                                }
                            });
                        }
                    }
                });
            }
        });
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        alertDialog.show();
    }

    // Display alert

    public void showAlert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();
    }

}

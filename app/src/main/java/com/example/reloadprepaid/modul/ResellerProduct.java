package com.example.reloadprepaid.modul;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rasyidi on 20/04/2016.
 */
public class ResellerProduct implements Parcelable {

    int id, oid;
    String code, description, amount, price, discount, saving;

    public ResellerProduct(int id, int oid, String code, String description, String amount, String price, String discount, String saving) {
        this.id = id;
        this.oid = oid;
        this.code = code;
        this.description = description;
        this.amount = amount;
        this.price = price;
        this.discount = discount;
        this.saving = saving;
    }

    protected ResellerProduct(Parcel in) {
        id = in.readInt();
        oid = in.readInt();
        code = in.readString();
        description = in.readString();
        amount = in.readString();
        price = in.readString();
        discount = in.readString();
        saving = in.readString();
    }

    public static final Creator<ResellerProduct> CREATOR = new Creator<ResellerProduct>() {
        @Override
        public ResellerProduct createFromParcel(Parcel in) {
            return new ResellerProduct(in);
        }

        @Override
        public ResellerProduct[] newArray(int size) {
            return new ResellerProduct[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOid() {
        return oid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSaving() {
        return saving;
    }

    public void setSaving(String saving) {
        this.saving = saving;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(oid);
        dest.writeString(code);
        dest.writeString(description);
        dest.writeString(amount);
        dest.writeString(price);
        dest.writeString(discount);
        dest.writeString(saving);
    }
}

package com.example.reloadprepaid.modul;

/**
 * Created by Rasyidi on 21/04/2016.
 */
public class HistoryData {

    String title, value;

    public HistoryData(String title, String value) {
        this.title = title;
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

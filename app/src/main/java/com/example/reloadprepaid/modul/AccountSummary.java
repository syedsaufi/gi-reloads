package com.example.reloadprepaid.modul;

import android.accounts.Account;

/**
 * Created by SyedSaufi on 20/06/2016.
 */
public class AccountSummary {

    String accountType, accountNo, accountName, accountBalance;

    public AccountSummary(){

    }

    public AccountSummary(String accountType, String accountNo, String accountName, String accountBalance) {
        this.accountType = accountType;
        this.accountNo = accountNo;
        this.accountName = accountName;
        this.accountBalance = accountBalance;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(String accountBalance) {
        this.accountBalance = accountBalance;
    }
}

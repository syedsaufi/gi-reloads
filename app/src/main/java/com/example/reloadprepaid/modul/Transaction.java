package com.example.reloadprepaid.modul;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SyedSaufi on 23/02/2016.
 */
public class Transaction implements Parcelable{

    String ref_no, operator, service, product, amount, recipient, description, datetime, status, phone_num, message;

    public Transaction(){

    }

    public Transaction(String ref_no, String operator, String service, String product, String amount, String recipient, String description, String datetime, String status, String phone_num, String message) {
        this.ref_no = ref_no;
        this.operator = operator;
        this.service = service;
        this.product = product;
        this.amount = amount;
        this.recipient = recipient;
        this.description = description;
        this.datetime = datetime;
        this.status = status;
        this.phone_num = phone_num;
        this.message = message;
    }

    protected Transaction(Parcel in) {
        ref_no = in.readString();
        operator = in.readString();
        service = in.readString();
        product = in.readString();
        amount = in.readString();
        recipient = in.readString();
        description = in.readString();
        datetime = in.readString();
        status = in.readString();
        phone_num = in.readString();
        message = in.readString();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public String getRef_no() {
        return ref_no;
    }

    public void setRef_no(String ref_no) {
        this.ref_no = ref_no;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ref_no);
        dest.writeString(operator);
        dest.writeString(service);
        dest.writeString(product);
        dest.writeString(amount);
        dest.writeString(recipient);
        dest.writeString(description);
        dest.writeString(datetime);
        dest.writeString(status);
        dest.writeString(phone_num);
        dest.writeString(message);
    }
}

package com.example.reloadprepaid.modul;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SyedSaufi on 11/02/2016.
 */
public class Product implements Parcelable {

    int id;
    String code, amount, description;

    public Product(){

    }

    public Product(int id, String code, String amount, String description) {
        this.id = id;
        this.code = code;
        this.amount = amount;
        this.description = description;
    }

    protected Product(Parcel in) {
        id = in.readInt();
        code = in.readString();
        amount = in.readString();
        description = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(code);
        dest.writeString(amount);
        dest.writeString(description);
    }
}

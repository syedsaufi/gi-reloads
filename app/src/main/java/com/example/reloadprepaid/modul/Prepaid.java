package com.example.reloadprepaid.modul;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by SyedSaufi on 10/02/2016.
 */
public class Prepaid implements Parcelable {


    String telcoID, tecoCode, telcoName;
    ArrayList<Product> productArrayList;

    public Prepaid(){

    }

    public Prepaid(String telcoID, String tecoCode, String telcoName, ArrayList<Product> productArrayList) {
        this.telcoID = telcoID;
        this.tecoCode = tecoCode;
        this.telcoName = telcoName;
        this.productArrayList = productArrayList;
    }

    protected Prepaid(Parcel in) {
        telcoID = in.readString();
        tecoCode = in.readString();
        telcoName = in.readString();
    }

    public static final Creator<Prepaid> CREATOR = new Creator<Prepaid>() {
        @Override
        public Prepaid createFromParcel(Parcel in) {
            return new Prepaid(in);
        }

        @Override
        public Prepaid[] newArray(int size) {
            return new Prepaid[size];
        }
    };

    public String getTelcoID() {
        return telcoID;
    }

    public void setTelcoID(String telcoID) {
        this.telcoID = telcoID;
    }

    public String getTecoCode() {
        return tecoCode;
    }

    public void setTecoCode(String tecoCode) {
        this.tecoCode = tecoCode;
    }

    public String getTelcoName() {
        return telcoName;
    }

    public void setTelcoName(String telcoName) {
        this.telcoName = telcoName;
    }

    public ArrayList<Product> getProductArrayList() {
        return productArrayList;
    }

    public void setProductArrayList(ArrayList<Product> productArrayList) {
        this.productArrayList = productArrayList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(telcoID);
        dest.writeString(tecoCode);
        dest.writeString(telcoName);
    }
}

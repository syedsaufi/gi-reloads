package com.example.reloadprepaid.modul;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Rasyidi on 20/04/2016.
 */
public class ResellerOperator implements Parcelable {

    String operator, operatorName;
    ArrayList<ResellerProduct> productArrayList;

    public ResellerOperator(String operator, String operatorName, ArrayList<ResellerProduct> productArrayList) {
        this.operator = operator;
        this.operatorName = operatorName;
        this.productArrayList = productArrayList;
    }

    protected ResellerOperator(Parcel in) {
        operator = in.readString();
        operatorName = in.readString();
        productArrayList = in.createTypedArrayList(ResellerProduct.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(operator);
        dest.writeString(operatorName);
        dest.writeTypedList(productArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResellerOperator> CREATOR = new Creator<ResellerOperator>() {
        @Override
        public ResellerOperator createFromParcel(Parcel in) {
            return new ResellerOperator(in);
        }

        @Override
        public ResellerOperator[] newArray(int size) {
            return new ResellerOperator[size];
        }
    };

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public ArrayList<ResellerProduct> getProductArrayList() {
        return productArrayList;
    }

    public void setProductArrayList(ArrayList<ResellerProduct> productArrayList) {
        this.productArrayList = productArrayList;
    }
}

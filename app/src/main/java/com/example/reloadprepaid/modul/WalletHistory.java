package com.example.reloadprepaid.modul;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by SyedSaufi on 15/02/2016.
 */
public class WalletHistory implements Parcelable {

    String refNo, service, product, unit, amount, refAccount, description, status;
    boolean accountTopup;
    Date dateTime;

    public WalletHistory(String refNo, String service, String product, String unit, String amount, String refAccount, String description, String status, boolean accountTopup, Date dateTime) {
        this.refNo = refNo;
        this.service = service;
        this.product = product;
        this.unit = unit;
        this.amount = amount;
        this.refAccount = refAccount;
        this.description = description;
        this.status = status;
        this.accountTopup = accountTopup;
        this.dateTime = dateTime;
    }

    protected WalletHistory(Parcel in) {
        refNo = in.readString();
        service = in.readString();
        product = in.readString();
        unit = in.readString();
        amount = in.readString();
        refAccount = in.readString();
        description = in.readString();
        status = in.readString();
        accountTopup = in.readByte() != 0;
    }

    public static final Creator<WalletHistory> CREATOR = new Creator<WalletHistory>() {
        @Override
        public WalletHistory createFromParcel(Parcel in) {
            return new WalletHistory(in);
        }

        @Override
        public WalletHistory[] newArray(int size) {
            return new WalletHistory[size];
        }
    };

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRefAccount() {
        return refAccount;
    }

    public void setRefAccount(String refAccount) {
        this.refAccount = refAccount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isAccountTopup() {
        return accountTopup;
    }

    public void setAccountTopup(boolean accountTopup) {
        this.accountTopup = accountTopup;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(refNo);
        dest.writeString(service);
        dest.writeString(product);
        dest.writeString(unit);
        dest.writeString(amount);
        dest.writeString(refAccount);
        dest.writeString(description);
        dest.writeString(status);
        dest.writeByte((byte) (accountTopup ? 1 : 0));
    }
}

package com.example.reloadprepaid.modul;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by SyedSaufi on 24/02/2016.
 */
public class PrepaidHistory implements Parcelable{
    String refNo, operator, service, product, amount, recipient, description, processBy, status;
    Date processTime, datetime, lastUpdate;

    public PrepaidHistory(String refNo, String operator, String service, String product, String amount, String recipient, String description, String processBy, String status, Date processTime, Date datetime, Date lastUpdate) {
        this.refNo = refNo;
        this.operator = operator;
        this.service = service;
        this.product = product;
        this.amount = amount;
        this.recipient = recipient;
        this.description = description;
        this.processBy = processBy;
        this.status = status;
        this.processTime = processTime;
        this.datetime = datetime;
        this.lastUpdate = lastUpdate;
    }

    protected PrepaidHistory(Parcel in) {
        refNo = in.readString();
        operator = in.readString();
        service = in.readString();
        product = in.readString();
        amount = in.readString();
        recipient = in.readString();
        description = in.readString();
        processBy = in.readString();
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(refNo);
        dest.writeString(operator);
        dest.writeString(service);
        dest.writeString(product);
        dest.writeString(amount);
        dest.writeString(recipient);
        dest.writeString(description);
        dest.writeString(processBy);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PrepaidHistory> CREATOR = new Creator<PrepaidHistory>() {
        @Override
        public PrepaidHistory createFromParcel(Parcel in) {
            return new PrepaidHistory(in);
        }

        @Override
        public PrepaidHistory[] newArray(int size) {
            return new PrepaidHistory[size];
        }
    };

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProcessBy() {
        return processBy;
    }

    public void setProcessBy(String processBy) {
        this.processBy = processBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Date processTime) {
        this.processTime = processTime;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}

package com.example.reloadprepaid.modul;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SyedSaufi on 16/02/2016.
 */
public class Favourite implements Parcelable{
    int id;
    String phone_no, name;

    public Favourite(){

    }

    public Favourite(int id, String phone_no, String name) {
        this.id = id;
        this.phone_no = phone_no;
        this.name = name;
    }

    protected Favourite(Parcel in) {
        id = in.readInt();
        phone_no = in.readString();
        name = in.readString();
    }

    public static final Creator<Favourite> CREATOR = new Creator<Favourite>() {
        @Override
        public Favourite createFromParcel(Parcel in) {
            return new Favourite(in);
        }

        @Override
        public Favourite[] newArray(int size) {
            return new Favourite[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(phone_no);
        dest.writeString(name);
    }
}

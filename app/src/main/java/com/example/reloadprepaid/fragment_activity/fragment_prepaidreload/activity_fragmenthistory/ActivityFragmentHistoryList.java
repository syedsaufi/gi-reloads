package com.example.reloadprepaid.fragment_activity.fragment_prepaidreload.activity_fragmenthistory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.HistoryDetailsAdapter;

public class ActivityFragmentHistoryList extends AppCompatActivity {

    ListView lvHistoryDetails;
    HistoryDetailsAdapter historyDetailsAdapter;
    String[] arrayLabel;
    String[] arrayValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_fragment_history_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Transaction History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initializeComponents();
    }

    private void initializeComponents() {

        arrayLabel = getIntent().getStringArrayExtra("arrayLabel");
        arrayValue = getIntent().getStringArrayExtra("arrayValue");
        lvHistoryDetails = (ListView) findViewById(R.id.lvHistoryDetail);
        historyDetailsAdapter = new HistoryDetailsAdapter(ActivityFragmentHistoryList.this, arrayLabel, arrayValue);
        lvHistoryDetails.setAdapter(historyDetailsAdapter);
    }

}

package com.example.reloadprepaid.fragment_activity.fragment_prepaidreload;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.PrepaidHistoryAdapter;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.fragment_activity.fragment_prepaidreload.activity_fragmenthistory.ActivityFragmentHistoryList;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.PrepaidHistory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHistoryPrepaidReload extends Fragment implements AdapterView.OnItemClickListener {

    ListView lvHistoryPrepaid;
    PrepaidHistoryAdapter prepaidHistoryAdapter;
    AllAccess allAccess;
    ProgressDialog progressDialog;


    public FragmentHistoryPrepaidReload() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_history_prepaid_reload, container, false);
        allAccess = (AllAccess) getActivity().getApplication();

        lvHistoryPrepaid = (ListView) view.findViewById(R.id.lvHistoryPrepaid);
        prepaidHistoryAdapter = new PrepaidHistoryAdapter(getActivity());
        lvHistoryPrepaid.setAdapter(prepaidHistoryAdapter);
        lvHistoryPrepaid.setOnItemClickListener(this);

        readAPI();

        return view;
    }

    private void readAPI() {
        progressDialog = ProgressDialog.show(getActivity(), "Loading", "Please wait..", true);
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Call<ResponseBody> result = client.getHistory(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()) {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        JSONObject resultObj = jsonObject.getJSONObject("result");
                        JSONArray transactionArray = resultObj.getJSONArray("transaction");

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        for (int i = 0; i < transactionArray.length(); i++) {
                            JSONObject transactionObj = transactionArray.getJSONObject(i);

                            Date processTime = null;
                            Date lastUpdate = null;
                            String processBy = null;
                            if (transactionObj.has("process_time")) {
                                processTime = sdf.parse(transactionObj.getString("process_time"));
                                lastUpdate = sdf.parse(transactionObj.getString("last_update"));
                                processBy = transactionObj.getString("process_by");
                            }
                            Date datetime = sdf.parse(transactionObj.getString("datetime"));
                            PrepaidHistory prepaidHistory = new PrepaidHistory(transactionObj.getString("ref_no"),
                                    transactionObj.getString("operator"),
                                    transactionObj.getString("service"),
                                    transactionObj.getString("product"),
                                    transactionObj.getString("amount"),
                                    transactionObj.getString("recipient"),
                                    transactionObj.getString("description"),
                                    processBy,
                                    transactionObj.getString("status"),
                                    processTime,
                                    datetime,
                                    lastUpdate);
                            prepaidHistoryAdapter.getPrepaidHistoryArrayList().add(prepaidHistory);
                        }
                        prepaidHistoryAdapter.notifyDataSetChanged();
                    }
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        PrepaidHistory prepaidHistory = prepaidHistoryAdapter.getPrepaidHistoryArrayList().get(position);

        if (prepaidHistory.getProcessTime() == null) {
            String[] arrayLabel = {
                    "Status",
                    "Service",
                    "Ref No",
                    "Recipient",
                    "Operator",
                    "Product",
                    "Amount",
                    "Payment Date",
                    "Payment Time"
            };

            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyy");
            SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");

            String status;
            if (prepaidHistory.getStatus().equals("NEW")) {
                status = "New";
            } else if (prepaidHistory.getStatus().equals("PCS")) {
                status = "Processing";
            } else if (prepaidHistory.getStatus().equals("SNT")) {
                status = "Processed";
            } else if (prepaidHistory.getStatus().equals("SUC")) {
                status = "Success";
            } else {
                status = "Failed";
            }

            String[] arrayValue = {
                    status,
                    prepaidHistory.getService(),
                    prepaidHistory.getRefNo(),
                    prepaidHistory.getRecipient(),
                    prepaidHistory.getOperator(),
                    prepaidHistory.getDescription(),
                    "RM" + prepaidHistory.getAmount(),
                    sdfDate.format(prepaidHistory.getDatetime()),
                    sdfTime.format(prepaidHistory.getDatetime())
            };

            Intent intent = new Intent(getActivity(), ActivityFragmentHistoryList.class);
            intent.putExtra("arrayLabel", arrayLabel);
            intent.putExtra("arrayValue", arrayValue);
            startActivity(intent);
        } else {
            String[] arrayLabel = {
                    "Status",
                    "Service",
                    "Ref No",
                    "Recipient",
                    "Operator",
                    "Product",
                    "Amount",
                    "Payment Date",
                    "Payment Time",
                    "Process By",
                    "Process Date",
                    "Process Time"
            };

            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyy");
            SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");

            String status;
            if (prepaidHistory.getStatus().equals("NEW")) {
                status = "New";
            } else if (prepaidHistory.getStatus().equals("PCS")) {
                status = "Processing";
            } else if (prepaidHistory.getStatus().equals("SNT")) {
                status = "Processed";
            } else if (prepaidHistory.getStatus().equals("SUC")) {
                status = "Success";
            } else {
                status = "Failed";
            }

            String[] arrayValue = {
                    status,
                    prepaidHistory.getService(),
                    prepaidHistory.getRefNo(),
                    prepaidHistory.getRecipient(),
                    prepaidHistory.getOperator(),
                    prepaidHistory.getDescription(),
                    "RM" + prepaidHistory.getAmount(),
                    sdfDate.format(prepaidHistory.getDatetime()),
                    sdfTime.format(prepaidHistory.getDatetime()),
                    prepaidHistory.getProcessBy(),
                    sdfDate.format(prepaidHistory.getProcessTime()),
                    sdfTime.format(prepaidHistory.getProcessTime())
            };

            Intent intent = new Intent(getActivity(), ActivityFragmentHistoryList.class);
            intent.putExtra("arrayLabel", arrayLabel);
            intent.putExtra("arrayValue", arrayValue);
            startActivity(intent);
        }
    }
}

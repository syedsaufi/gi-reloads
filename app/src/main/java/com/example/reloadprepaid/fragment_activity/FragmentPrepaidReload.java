package com.example.reloadprepaid.fragment_activity;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.FavouriteAdapter;
import com.example.reloadprepaid.adapter.SpinnerChoicesAdapter;
import com.example.reloadprepaid.adapter.SpinnerFavouriteManualAdapter;
import com.example.reloadprepaid.adapter.SpinnerPrepaidAdapter;
import com.example.reloadprepaid.adapter.SpinnerProductAdapter;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.fragment_activity.activity_fragmentprepaidreload.ConfirmationPrepaidReloadActivity;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.Favourite;
import com.example.reloadprepaid.modul.Prepaid;
import com.example.reloadprepaid.modul.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPrepaidReload extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    View view;
    AllAccess allAccess;
    Spinner spinOperator, spinProduct, spinChoice, spinFavouriteManual;
    SpinnerPrepaidAdapter prepaidAdapter;
    SpinnerProductAdapter productAdapter;
    SpinnerChoicesAdapter choicesAdapter;
    SpinnerFavouriteManualAdapter favouriteManualAdapter;
    TableRow tbrFavourite, tbrManual, tbrEmptyFavourite;
    ProgressDialog progressDialog;
    Button btnContinueReload;
    EditText etPhoneNum;

    FavouriteAdapter favouriteAdapter;

    String operator, product, phone_num;

    public FragmentPrepaidReload() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_prepaid_reload, container, false);

        initializeComponents();
        readAPITelco();
        readAPIFavourite();

        return view;
    }

    private void initializeComponents() {
        allAccess = (AllAccess) getActivity().getApplication();
        favouriteAdapter = new FavouriteAdapter(getActivity());

        etPhoneNum = (EditText) view.findViewById(R.id.etPhoneNum);

        btnContinueReload = (Button) view.findViewById(R.id.btnContinueReload);
        btnContinueReload.setOnClickListener(this);

        tbrFavourite = (TableRow) view.findViewById(R.id.tbr2);
        tbrManual = (TableRow) view.findViewById(R.id.tbr3);
        tbrEmptyFavourite = (TableRow) view.findViewById(R.id.tbr4);

        String choiceArray[] = {"Favourite", "Insert number manually"};
        prepaidAdapter = new SpinnerPrepaidAdapter(getActivity(), null);
        choicesAdapter = new SpinnerChoicesAdapter(getActivity(), choiceArray);

        spinOperator = (Spinner) view.findViewById(R.id.spinOperator);
        spinProduct = (Spinner) view.findViewById(R.id.spinProduct);
        spinChoice = (Spinner) view.findViewById(R.id.spinChoice);
        spinFavouriteManual = (Spinner) view.findViewById(R.id.spinFavouriteManual);


        spinProduct.setAdapter(productAdapter);
        spinChoice.setAdapter(choicesAdapter);


        spinChoice.setOnItemSelectedListener(this);
    }

    private void readAPITelco() {
        progressDialog = ProgressDialog.show(getActivity(), "Loading", "Please wait...", true);
        APIClient client = ServiceGenerator.createService(APIClient.class);
        final Call<ResponseBody> result = client.getResellerPrice(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()) {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        JSONArray operatorArray = jsonObject.getJSONArray("result");
                        ArrayList<Prepaid> prepaidArrayList = new ArrayList<Prepaid>();
                        for (int i = 0; i < operatorArray.length(); i++) {
                            JSONObject operatorObj = operatorArray.getJSONObject(i);

                            JSONArray arrayProduct = operatorObj.getJSONArray("product");
                            ArrayList<Product> productList = new ArrayList<Product>();
                            for (int j = 0; j < arrayProduct.length(); j++) {
                                JSONObject productObj = arrayProduct.getJSONObject(j);
                                Product product = new Product(productObj.getInt("id"), productObj.getString("code"), productObj.getString("amount"), productObj.getString("description"));
                                productList.add(product);
                            }
                            Prepaid prepaid = new Prepaid("0", operatorObj.getString("operator"), operatorObj.getString("operator_name"), productList);
                            prepaidArrayList.add(prepaid);
                            prepaidAdapter.notifyDataSetChanged();

                            prepaidAdapter = new SpinnerPrepaidAdapter(getActivity(), prepaidArrayList);
                            prepaidAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinOperator.setAdapter(prepaidAdapter);
                            spinOperator.setSelection(0);
                            spinOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    productAdapter = new SpinnerProductAdapter(getActivity(), prepaidAdapter.getPrepaidList().get(position).getProductArrayList(), position);
                                    spinProduct.setAdapter(productAdapter);
                                    spinProduct.setSelection(0);

                                    operator = prepaidAdapter.getPrepaidList().get(position).getTecoCode();
                                    allAccess.setOperator(operator);
                                    spinProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            product = productAdapter.getProductList().get(position).getDescription();
                                            allAccess.setProduct(product);
                                            allAccess.setProduct_id(productAdapter.getProductList().get(position).getCode());
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        }
                        //Get data from JSON read operator
//                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
//                        JSONArray operatorArray = jsonObject.getJSONArray("result");
//                        ArrayList<Prepaid> prepaidArrayList = new ArrayList<Prepaid>();
//                        for (int i = 0; i < operatorArray.length(); i++) {
//                            JSONObject operatorObj = operatorArray.getJSONObject(i);
//
//                            JSONArray arrayProduct = operatorObj.getJSONArray("product");
//                            ArrayList<Product> productList = new ArrayList<Product>();
//                            for (int j = 0; j < arrayProduct.length(); j++){
//                                JSONObject productObj = arrayProduct.getJSONObject(j);
//                                Product product = new Product(productObj.getInt("id"), productObj.getString("code"), productObj.getString("amount"), productObj.getString("description"));
//                                productList.add(product);
//                            }
//                            Prepaid prepaid = new Prepaid("0", operatorObj.getString("operator"), operatorObj.getString("operator_name"), productList);
//                            prepaidArrayList.add(prepaid);
//                        }
//                        prepaidAdapter.notifyDataSetChanged();
//                        productAdapter.addAll(prepaidAdapter.getItem(0).getProductArrayList());

                        //get data from JSON read favourite
//                        JSONArray favouriteArray = resultObj.getJSONArray("favourite_no");
//                        for (int i = 0; i < favouriteArray.length(); i++) {
//                            JSONObject favouriteObj = favouriteArray.getJSONObject(i);
//                            Favourite favourite = new Favourite(favouriteObj.getInt("id"), favouriteObj.getString("phone_no"), favouriteObj.getString("name"));
//                            favouriteManualAdapter.add(favourite);
//                        }
//                        favouriteManualAdapter.notifyDataSetChanged();

                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        Log.e("asdasd", "oooof");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    private void readAPIFavourite() {
        Log.e("damasukfavourite", "test1");
//        progressDialog = ProgressDialog.show(getActivity(), "Loading", "Please wait..", true);
        favouriteAdapter.getFavouriteArrayList().clear();
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Call<ResponseBody> result = client.getFavourite(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()) {
                        Log.e("damasukfavourite", "test2");
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        JSONObject resultObj = jsonObject.getJSONObject("result");
                        JSONArray favouriteArray = resultObj.getJSONArray("favourite_no");
                        for (int i = 0; i < favouriteArray.length(); i++) {

                            JSONObject favouriteObj = favouriteArray.getJSONObject(i);
                            Favourite favourite = new Favourite(favouriteObj.getInt("id"),
                                    favouriteObj.getString("phone_no"),
                                    favouriteObj.getString("name"));

                            favouriteAdapter.getFavouriteArrayList().add(favourite);
                        }
                        favouriteManualAdapter = new SpinnerFavouriteManualAdapter(getActivity(), favouriteAdapter.getFavouriteArrayList());

                        spinFavouriteManual.setAdapter(favouriteManualAdapter);
                        favouriteAdapter.notifyDataSetChanged();

                        if (favouriteAdapter.getFavouriteArrayList().size() == 0) {
                            tbrFavourite.setVisibility(View.GONE);
                            tbrEmptyFavourite.setVisibility(View.VISIBLE);
                        } else {
                            tbrEmptyFavourite.setVisibility(View.GONE);
                            tbrFavourite.setVisibility(View.VISIBLE);
                        }
                        Log.e("damasukfavourite", "test4");
                        progressDialog.dismiss();
                        Log.e("damasukfavourite", "test5");
                    } else {
                        progressDialog.dismiss();
                        Log.e("xmasuk", "xmasuk");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("xmasukfavourite", "test1");
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spinOperator) {
            productAdapter.clear();
            productAdapter.addAll(prepaidAdapter.getItem(position).getProductArrayList());
            productAdapter.notifyDataSetChanged();
        } else if (parent.getId() == R.id.spinChoice) {
            if (position == 0) {
                if (favouriteAdapter.getFavouriteArrayList().size() == 0) {
                    tbrFavourite.setVisibility(View.GONE);
                    tbrEmptyFavourite.setVisibility(View.VISIBLE);
                    tbrManual.setVisibility(View.GONE);
                } else {
                    tbrFavourite.setVisibility(View.VISIBLE);
                    tbrManual.setVisibility(View.GONE);
                }

            } else if (position == 1) {
                tbrFavourite.setVisibility(View.GONE);
                tbrEmptyFavourite.setVisibility(View.GONE);
                tbrManual.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (!etPhoneNum.getText().toString().equals("")) {
            Intent intent = new Intent(getActivity(), ConfirmationPrepaidReloadActivity.class);
            intent.putExtra("prepaid", prepaidAdapter.getItem(spinOperator.getSelectedItemPosition()));
            intent.putExtra("product", productAdapter.getItem(spinProduct.getSelectedItemPosition()));
            if (spinChoice.getSelectedItemPosition() == 0) {
                intent.putExtra("phone_number", favouriteManualAdapter.getItem(spinFavouriteManual.getSelectedItemPosition()).getPhone_no());
            } else {
                intent.putExtra("phone_number", etPhoneNum.getText().toString().trim());
            }
            startActivity(intent);
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Please insert phone number before continue. You can choose from your Favourite or insert them manually.");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });
            alertDialog.show();
        }

    }
}

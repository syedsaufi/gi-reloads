package com.example.reloadprepaid.fragment_activity.activity_fragmentprepaidreload;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.fragment_activity.activity_fragmentprepaidreload.activity_activityconfirmationprepaidreload.ReloadResultActivity;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.Prepaid;
import com.example.reloadprepaid.modul.Product;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmationPrepaidReloadActivity extends AppCompatActivity {

    AllAccess allAccess;
    TextView tvConfirmOperator, tvConfirmProduct, tvConfirmPhoneNum;
    Prepaid prepaid;
    Product product;
    String phoneNumber;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_prepaid_reload);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Confirmation");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initializeComponents();
    }

    private void initializeComponents() {
        allAccess = (AllAccess) getApplication();

        prepaid = getIntent().getParcelableExtra("prepaid");
        product = getIntent().getParcelableExtra("product");
        phoneNumber = getIntent().getStringExtra("phone_number");

        tvConfirmOperator = (TextView) findViewById(R.id.tvConfirmOperator);
        tvConfirmProduct = (TextView) findViewById(R.id.tvConfirmProduct);
        tvConfirmPhoneNum = (TextView) findViewById(R.id.tvConfirmPhoneNumber);

        tvConfirmOperator.setText(prepaid.getTelcoName());
        tvConfirmProduct.setText(product.getDescription());
        tvConfirmPhoneNum.setText(phoneNumber);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickMakeTransaction(View view) {
        readAPI();

    }

    private void readAPI() {
        progressDialog = ProgressDialog.show(ConfirmationPrepaidReloadActivity.this, "Processing", "Please wait..", true);
        APIClient client = ServiceGenerator.createService(APIClient.class);
        final Call<ResponseBody> result = client.makeTransaction(allAccess.getAuth_key(), product.getCode(), phoneNumber);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()) {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        Log.e("json", jsonObject.toString());
                        JSONObject resultObj = jsonObject.getJSONObject("result");

                        String[] arrayLabel = {
                                "Service",
                                "Status",
                                "Description",
                                "Reference No",
                                "Operator",
                                "Phone No",
                                "Amount",
                                "Transaction Date",
                                "Transaction Time"
                        };

                        String status;
                        if (resultObj.getString("status").equals("NEW")) {
                            status = "New";
                        } else if (resultObj.getString("status").equals("PCS")) {
                            status = "Processing";
                        } else if (resultObj.getString("status").equals("SNT")) {
                            status = "Processed";
                        } else if (resultObj.getString("status").equals("SUC")) {
                            status = "Success";
                        } else {
                            status = "Failed";
                        }

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date datetime = sdf.parse(resultObj.getString("datetime"));

                        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyy");
                        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");

                        String[] arrayValue = {
                                resultObj.getString("service"),
                                status,
                                resultObj.getString("description"),
                                resultObj.getString("ref_no"),
                                resultObj.getString("operator"),
                                resultObj.getString("recipient"),
                                "RM" + resultObj.getString("amount"),
                                sdfDate.format(datetime),
                                sdfTime.format(datetime)
                        };

                        Intent intent = new Intent(ConfirmationPrepaidReloadActivity.this, ReloadResultActivity.class);
                        intent.putExtra("arrayLabel", arrayLabel);
                        intent.putExtra("arrayValue", arrayValue);
                        intent.putExtra("status", resultObj.getString("status").equals("NEW") ? true : false);
                        startActivity(intent);
                        progressDialog.dismiss();
                        finish();
                    } else {
                        JSONObject jsonObject = new JSONObject(new String(response.errorBody().string()));
                        showAlert("Error", jsonObject.getString("message"));
                        progressDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    // Alert Dialog

    public void showAlert(String title, String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();
    }
}

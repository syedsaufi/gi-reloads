package com.example.reloadprepaid.fragment_activity.activity_fragmentfavourite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.Favourite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityFragmentFavourite extends AppCompatActivity {

    AllAccess allAccess;
    TextView tvMobileNumber;
    EditText etMobileNumber, etMobileName;
    Favourite favourite;
    ProgressDialog progressDialog;
    Button btnAddFavourite, btnUpdateFavourite;
    TableRow tbrFavouriteAdd, tbrFavouriteUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_fragment_favourite);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeComponents();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initializeComponents() {
        allAccess = (AllAccess) getApplication();

        tvMobileNumber = (TextView) findViewById(R.id.tvFavouriteMobileNo);
        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        etMobileName = (EditText) findViewById(R.id.etMobileName);
        btnAddFavourite = (Button) findViewById(R.id.btnAddFavourite);
        btnUpdateFavourite = (Button) findViewById(R.id.btnUpdateFavourite);
        tbrFavouriteAdd = (TableRow) findViewById(R.id.tbrFavouriteAdd);
        tbrFavouriteUpdate = (TableRow) findViewById(R.id.tbrFavouriteUpdate);
        String flag = getIntent().getStringExtra("flag");

        if (flag.equals("exist")) {
            setTitle("Update Favourite");

            favourite = getIntent().getParcelableExtra("favourite");

            tbrFavouriteAdd.setVisibility(View.GONE);
            tbrFavouriteUpdate.setVisibility(View.VISIBLE);
            btnAddFavourite.setVisibility(View.GONE);
            btnUpdateFavourite.setVisibility(View.VISIBLE);

            tvMobileNumber.setText(favourite.getPhone_no());
            etMobileName.setText(favourite.getName());
        } else if (flag.equals("new")) {
            setTitle("Add Favourite");

            tbrFavouriteAdd.setVisibility(View.VISIBLE);
            tbrFavouriteUpdate.setVisibility(View.GONE);
            btnAddFavourite.setVisibility(View.VISIBLE);
            btnUpdateFavourite.setVisibility(View.GONE);

            etMobileNumber.setText("");
            etMobileName.setText("");
        }
    }

    public Boolean checkEmpty() {
        if (etMobileName.getText().toString().equals("") || etMobileNumber.getText().toString().equals("")) {
            AlertDialog alertDialog = new AlertDialog.Builder(ActivityFragmentFavourite.this).create();
            alertDialog.setTitle("Empty");
            alertDialog.setMessage("Please fill in all the field before adding favourite.");
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });
            alertDialog.show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickAddFavourite(View view) {
        if (checkEmpty()) {
            progressDialog = ProgressDialog.show(ActivityFragmentFavourite.this, "Adding New Favourite", "Please Wait..", true);
            APIClient client = ServiceGenerator.createService(APIClient.class);
            Call<ResponseBody> result = client.addFavourite(allAccess.getAuth_key(), etMobileNumber.getText().toString(), etMobileName.getText().toString());
            result.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivityFragmentFavourite.this).create();
                    alertDialog.setTitle("Success");
                    alertDialog.setTitle("New favourite have been add.");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        }
                    });
                    alertDialog.show();
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

        }

    }

//    public void onClickUpdateFavourite(View view) {
//        progressDialog = ProgressDialog.show(ActivityFragmentFavourite.this, "Updating Favourite", "Please Wait..", true);
//        APIClient client = ServiceGenerator.createService(APIClient.class);
//        Log.e("FNO", favourite.getPhone_no());
//        Log.e("FNA", etMobileName.getText().toString());
//        Call<ResponseBody> result = client.updateFavourite(allAccess.getAuth_key(), favourite.getPhone_no(), etMobileName.getText().toString());
//        result.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Response<ResponseBody> response) {
//                if (response.isSuccess()) {
//                    progressDialog.dismiss();
//                    AlertDialog alertDialog = new AlertDialog.Builder(ActivityFragmentFavourite.this).create();
//                    alertDialog.setTitle("Success");
//                    alertDialog.setTitle("Update successful.");
//                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Intent intent = new Intent();
//                            setResult(Activity.RESULT_OK, intent);
//                            finish();
//                        }
//                    });
//                    alertDialog.show();
//                } else {
//                    progressDialog.dismiss();
//                    AlertDialog alertDialog = new AlertDialog.Builder(ActivityFragmentFavourite.this).create();
//                    alertDialog.setTitle("FAIL");
//                    alertDialog.setTitle("We are having difficulty at the time, please try again later.");
//                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            return;
//                        }
//                    });
//                    alertDialog.show();
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//
//            }
//        });
//    }
}

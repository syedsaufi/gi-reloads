package com.example.reloadprepaid.fragment_activity.activity_setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChangePassword extends AppCompatActivity {

    AllAccess allAccess;
    EditText etOldPassword, etNewPassword, etVerify;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Change Password");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initializeComponents();
    }

    private void initializeComponents() {
        allAccess = (AllAccess) getApplication();
        etOldPassword = (EditText) findViewById(R.id.etcpOldPassword);
        etNewPassword = (EditText) findViewById(R.id.etcpNewPassword);
        etVerify = (EditText) findViewById(R.id.etcpVerify);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean verifyInput() {
        if (etNewPassword.getText().toString().equals("") || etNewPassword.getText().toString().equals("")) {
            displayAlert("Empty field", "You need to fill in all the required field to continue");
            return false;
        }
        else if (!(etVerify.getText().toString().equals(etNewPassword.getText().toString()))){
            displayAlert("Wrong password", "Your password verification should be the same as the password you have just filled.");
            return false;
        }
        return true;
    }

    public void displayAlert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        alertDialog.show();
    }

    public void onClickChangePassword(View view) {
        if (verifyInput()) {
            progressDialog = ProgressDialog.show(ActivityChangePassword.this, "Updating Information", "Please wait..", true);
            APIClient client = ServiceGenerator.createService(APIClient.class);
            Call<ResponseBody> result = client.getChangePassword(allAccess.getAuth_key(), etOldPassword.getText().toString(), etNewPassword.getText().toString());
            result.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivityChangePassword.this).create();
                    alertDialog.setTitle("Success");
                    alertDialog.setMessage("Change password successful!");
                    alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialog.show();
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
    }
}

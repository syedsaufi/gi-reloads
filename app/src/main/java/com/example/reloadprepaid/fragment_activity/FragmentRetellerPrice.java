package com.example.reloadprepaid.fragment_activity;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.PrepaidAdapter;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.ResellerOperator;
import com.example.reloadprepaid.modul.ResellerProduct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentRetellerPrice extends Fragment {

    ExpandableListView xlvPrepaid;
    AllAccess allAccess;
    PrepaidAdapter prepaidAdapter;
    ProgressDialog progressDialog;

    public FragmentRetellerPrice() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fragment_reteller_price, container, false);

        progressDialog = ProgressDialog.show(getActivity(), "Loading", "Please wait..", true);

        allAccess = (AllAccess) getActivity().getApplication();
        xlvPrepaid = (ExpandableListView) view.findViewById(R.id.xlvPrepaid);
        prepaidAdapter = new PrepaidAdapter(getActivity());
        xlvPrepaid.setAdapter(prepaidAdapter);
        readAPI();


        return view;
    }

    private void readAPI() {
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Call<ResponseBody> result = client.getResellerPrice(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()){
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        JSONArray jsonArrayOperator = jsonObject.getJSONArray("result");

                        for (int i = 0; i < jsonArrayOperator.length(); i++) {
                            JSONObject jsonObjectOperator = jsonArrayOperator.getJSONObject(i);
                            JSONArray jsonArrayProduct = jsonObjectOperator.getJSONArray("product");
                            ArrayList<ResellerProduct> productArrayList = new ArrayList<>();
                            for (int j = 0; j < jsonArrayProduct.length(); j++) {
                                JSONObject jsonObjectProduct = jsonArrayProduct.getJSONObject(j);
                                ResellerProduct resellerProduct = new ResellerProduct(jsonObjectProduct.getInt("id"),
                                        jsonObjectProduct.getInt("oid"),
                                        jsonObjectProduct.getString("code"),
                                        jsonObjectProduct.getString("description"),
                                        jsonObjectProduct.getString("amount"),
                                        jsonObjectProduct.getString("price"),
                                        jsonObjectProduct.getString("discount"),
                                        jsonObjectProduct.getString("saving"));
                                productArrayList.add(resellerProduct);
                            }
                            ResellerOperator resellerOperator = new ResellerOperator(jsonObjectOperator.getString("operator"),
                                    jsonObjectOperator.getString("operator_name"),
                                    productArrayList);
                            prepaidAdapter.getResellerOperatorArrayList().add(resellerOperator);
                        }
                        prepaidAdapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

}

package com.example.reloadprepaid.fragment_activity.activity_fragmentprepaidreload.activity_activityconfirmationprepaidreload;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.TransactionAdapter;
import com.example.reloadprepaid.application.AllAccess;
import com.squareup.picasso.Picasso;

public class ReloadResultActivity extends AppCompatActivity {

    AllAccess allaccess;
    ListView lvTransaction;
    TransactionAdapter transactionAdapter;
    TextView txtvStatus;
    ImageView imgvStatus;
    String[] arrayLabel;
    String[] arrayValue;
    Boolean status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reload_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Acknowledgment");
        initializeComponents();
    }

    private void initializeComponents() {
        allaccess = (AllAccess) getApplication();
        arrayLabel = getIntent().getStringArrayExtra("arrayLabel");
        arrayValue = getIntent().getStringArrayExtra("arrayValue");
        status = getIntent().getBooleanExtra("status", false);

        transactionAdapter = new TransactionAdapter(ReloadResultActivity.this, arrayLabel, arrayValue);
        lvTransaction = (ListView) findViewById(R.id.lvTransaction);
        lvTransaction.setAdapter(transactionAdapter);

        txtvStatus = (TextView) findViewById(R.id.txtvStatus);
        imgvStatus = (ImageView) findViewById(R.id.imgvStatus);
        if (status) {
            Picasso.with(this).load(R.drawable.newprocess).into(imgvStatus);
            txtvStatus.setText("Your reload is accepted and will be process shortly");
        } else {
            Picasso.with(this).load(R.drawable.failed).into(imgvStatus);
            txtvStatus.setText("Your reload is not accepted. Please try again later");
        }
    }

    public void newPrepaidReload(View view) {
        finish();
    }
}

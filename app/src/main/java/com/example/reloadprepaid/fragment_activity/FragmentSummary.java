package com.example.reloadprepaid.fragment_activity;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.reloadprepaid.HomeActivity;
import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.SummaryAdapter;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.AccountSummary;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSummary extends Fragment {

    AllAccess allAccess;
    TextView txtvFullname, txtvEmail, txtvMobile;
    View view;
    ProgressDialog progressDialog;
    SummaryAdapter summaryAdapter;
    ListView lvAccountSummary;


    public FragmentSummary() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_summary, container, false);

        initializeComponents();
        readAPI();
        return view;
    }

    private void readAPI() {
        progressDialog = ProgressDialog.show(getActivity(), "Loading", "Please wait..", true);
        APIClient client = ServiceGenerator.createService(APIClient.class);
        final Call<ResponseBody> result = client.getDashboard(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {

                try {
                    if (response.isSuccess()) {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));

                        //get profile
                        txtvFullname.setText(jsonObject.getString("name"));
                        txtvEmail.setText(jsonObject.getString("email"));
                        txtvMobile.setText(jsonObject.getString("mobile"));

                        JSONArray walletArray = jsonObject.getJSONArray("wallet");
                        for (int i = 0; i < walletArray.length(); i++) {
                            JSONObject walletObj = walletArray.getJSONObject(i);
                            AccountSummary accountSummary = new AccountSummary(walletObj.has("acc_type") ? walletObj.getString("acc_type") : null,
                                    walletObj.has("acc_no") ? walletObj.getString("acc_no") : null,
                                    walletObj.has("acc_name") ? walletObj.getString("acc_name") : null,
                                    walletObj.getString("unit") + walletObj.getString("acc_balance"));

                            summaryAdapter.getAccountArrayList().add(accountSummary);

                            if (walletObj.getString("acc_type").equals("DEALER")){
                                summaryAdapter.setCheckDealer(true);
                            }

                            summaryAdapter.notifyDataSetChanged();
                        }


                    } else {
                        JSONObject jsonObject = new JSONObject(new String(response.errorBody().string()));
                        Log.e("code", jsonObject.getString("code"));
                        Log.e("message", jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                Log.e("eror", "netwok perobolom");
            }
        });
    }

    public void initializeComponents() {
        allAccess = (AllAccess) getActivity().getApplication();
        txtvFullname = (TextView) view.findViewById(R.id.txtvFullname);
        txtvEmail = (TextView) view.findViewById(R.id.txtvEmail);
        txtvMobile = (TextView) view.findViewById(R.id.txtvMobile);

        summaryAdapter = new SummaryAdapter(getContext(), allAccess.getAuth_key());
        lvAccountSummary = (ListView) view.findViewById(R.id.lvAccountSummary);
        lvAccountSummary.setAdapter(summaryAdapter);

        txtvFullname.setText("-");
        txtvEmail.setText("-");
        txtvMobile.setText("-");

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, new IntentFilter("update_balance"));
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("dapat?", "dapat");
            summaryAdapter.getAccountArrayList().clear();
            readAPI();
        }
    };

}

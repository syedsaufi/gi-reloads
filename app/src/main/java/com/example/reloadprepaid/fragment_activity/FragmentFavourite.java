package com.example.reloadprepaid.fragment_activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.FavouriteAdapter;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.Favourite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFavourite extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    AllAccess allAccess;
    ListView lvFavourite;
    FavouriteAdapter favouriteAdapter;
    View view;
    ProgressDialog progressDialog;

    public FragmentFavourite() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_favourite, container, false);

        initComponents();
        readAPI();

        return view;
    }

    private void initComponents() {


        allAccess = (AllAccess) getActivity().getApplication();
        lvFavourite = (ListView) view.findViewById(R.id.lvFavourite);
        FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fabAddFavourite);
        floatingActionButton.setOnClickListener(FragmentFavourite.this);

        favouriteAdapter = new FavouriteAdapter(getActivity());
        lvFavourite.setAdapter(favouriteAdapter);
        lvFavourite.setOnItemClickListener(FragmentFavourite.this);
        lvFavourite.setOnItemLongClickListener(FragmentFavourite.this);
    }

    @Override
    public void onClick(View v) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_favourite_update, null);
        final EditText etxtMobile = (EditText) dialogView.findViewById(R.id.etxtMobile);
        final EditText etxtName = (EditText) dialogView.findViewById(R.id.etxtName);

        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setTitle("Add New Favourite Number")
                .setPositiveButton("Save", null)
                .setNegativeButton("Cancel", null)
                .create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (etxtMobile.getText().toString().trim().equals("") || etxtName.getText().toString().trim().equals("")) {
                            showAlert("Error", "Please enter title");
                        } else {
                            progressDialog = ProgressDialog.show(getActivity(), "Adding New Favourite Number", "Please Wait..", true);
                            APIClient client = ServiceGenerator.createService(APIClient.class);
                            Call<ResponseBody> result = client.addFavourite(allAccess.getAuth_key(), etxtName.getText().toString(), etxtMobile.getText().toString());
                            result.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Response<ResponseBody> response) {
                                    progressDialog.dismiss();
                                    if (response.isSuccess()) {
                                        alertDialog.dismiss();
                                        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                                        alertDialog.setTitle("Success");
                                        alertDialog.setMessage("New favourite number have been add.");
                                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                readAPI();
                                            }
                                        });
                                        alertDialog.show();
                                    } else {
                                        showAlert("Failed", "Please try again");
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    showAlert("Network error", "Please try again");
                                }
                            });
                        }
                    }
                });
            }
        });
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        alertDialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Favourite favourite = favouriteAdapter.getFavouriteArrayList().get(position);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_favourite_update, null);
        final EditText etxtMobile = (EditText) dialogView.findViewById(R.id.etxtMobile);
        final EditText etxtName = (EditText) dialogView.findViewById(R.id.etxtName);
        etxtMobile.setText(favourite.getPhone_no());
        etxtMobile.setEnabled(false);
        etxtName.setText(favourite.getName());

        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setTitle("Add New Favourite Number")
                .setPositiveButton("Save", null)
                .setNegativeButton("Cancel", null)
                .create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (etxtMobile.getText().toString().trim().equals("") || etxtName.getText().toString().trim().equals("")) {
                            showAlert("Error", "Please enter title");
                        } else {
                            progressDialog = ProgressDialog.show(getActivity(), "Updating Favourite", "Please Wait..", true);
                            APIClient client = ServiceGenerator.createService(APIClient.class);
                            Call<ResponseBody> result = client.editFavourite(String.valueOf(favourite.getId()), allAccess.getAuth_key(), etxtName.getText().toString(), favourite.getPhone_no());
                            result.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Response<ResponseBody> response) {
                                    progressDialog.dismiss();
                                    if (response.isSuccess()) {
                                        alertDialog.dismiss();
                                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                                        alertDialog.setTitle("Success");
                                        alertDialog.setMessage("Update successful.");
                                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                readAPI();
                                            }
                                        });
                                        alertDialog.show();
                                    } else {
                                        showAlert("Failed", "Please try again");
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    showAlert("Network error", "Please try again");
                                }
                            });
                        }
                    }
                });
            }
        });
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                readAPI();
            }
        } else if (requestCode == 102) {
            if (resultCode == Activity.RESULT_OK) {
                readAPI();
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Delete");
        alertDialog.setMessage("Delete " + favouriteAdapter.getFavouriteArrayList().get(position).getName() + " ?");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressDialog = ProgressDialog.show(getActivity(), "Delete", "Deleting favourite number..", true);
                APIClient client = ServiceGenerator.createService(APIClient.class);
                Call<ResponseBody> result = client.deleteFavourite(String.valueOf(favouriteAdapter.getFavouriteArrayList().get(position).getId()), allAccess.getAuth_key());
                result.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Response<ResponseBody> response) {
                        progressDialog.dismiss();
                        readAPI();
                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setTitle("Delete");
                        alertDialog.setMessage("Delete successful");
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        });
                        alertDialog.show();
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        alertDialog.show();
        return true;
    }

    private void readAPI() {
        progressDialog = ProgressDialog.show(getActivity(), "Loading", "Please wait..", true);
        favouriteAdapter.getFavouriteArrayList().clear();
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Call<ResponseBody> result = client.getFavourite(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()){
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        JSONObject resultObj = jsonObject.getJSONObject("result");
                        JSONArray favouriteArray = resultObj.getJSONArray("favourite_no");
                        for (int i = 0; i < favouriteArray.length(); i++) {

                            JSONObject favouriteObj = favouriteArray.getJSONObject(i);
                            Favourite favourite = new Favourite(favouriteObj.getInt("id"),
                                    favouriteObj.getString("phone_no"),
                                    favouriteObj.getString("name"));

                            favouriteAdapter.getFavouriteArrayList().add(favourite);
                        }
                        favouriteAdapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        Log.e("xmasuk", "xmasuk");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    // Display alert

    public void showAlert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();
    }
}

package com.example.reloadprepaid.fragment_activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.SettingAdapter;
import com.example.reloadprepaid.fragment_activity.activity_setting.ActivityChangePassword;
import com.example.reloadprepaid.fragment_activity.activity_setting.ActivityUpdateProfile;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSetting extends Fragment implements AdapterView.OnItemClickListener{

    View view;
    ListView lvSettings;
    SettingAdapter settingAdapter;
    
    public FragmentSetting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_setting, container, false);
        initializeComponents();
        return view;
    }

    private void initializeComponents() {
        lvSettings = (ListView) view.findViewById(R.id.lvSetting);
        settingAdapter = new SettingAdapter(getActivity());
        lvSettings.setAdapter(settingAdapter);
        lvSettings.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0){
            startActivity(new Intent(getActivity(), ActivityUpdateProfile.class));
        } else if (position == 1){
            startActivity(new Intent(getActivity(), ActivityChangePassword.class));
        }
    }
}

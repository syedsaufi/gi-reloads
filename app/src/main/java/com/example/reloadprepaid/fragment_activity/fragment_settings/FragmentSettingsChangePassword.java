package com.example.reloadprepaid.fragment_activity.fragment_settings;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSettingsChangePassword extends Fragment {


    Button btnChangePassword;
    EditText etOldPass, etNewPass, etVeryPass;
    AllAccess allaccess;
    View view;

    public FragmentSettingsChangePassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_settings_change_password, container, false);

        initializeComp();
        onClickChangePassword();
        return view;
    }

    public void initializeComp() {
        allaccess = (AllAccess) getActivity().getApplication();
        btnChangePassword = (Button) view.findViewById(R.id.btnChangePassword);
        etOldPass = (EditText) view.findViewById(R.id.etOldPassword);
        etNewPass = (EditText) view.findViewById(R.id.etNewPassword);
        etVeryPass = (EditText) view.findViewById(R.id.etNewPasswordVerify);
    }

    public boolean verification(){

        String oldPassword = etOldPass.getText().toString();
        String newPassword = etNewPass.getText().toString();
        String veryPass = etVeryPass.getText().toString();

        if (oldPassword.equals("") || newPassword.equals("") || veryPass.equals("")) {
            showDialog("Empty field", "You need to fill in all the field to complete your password change");
            return false;
        }
        if (!(veryPass.equals(newPassword))) {
            showDialog("Wrong password", "Your password verification should be the same as the password you have just filled.");
            return false;
        }

        return true;
    }

    public void showDialog(String title, String message){
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        alertDialog.show();
    }

    public void onClickChangePassword(){
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ch", "ch1");
                if (verification()){
                    Log.e("ch", "ch2");
                    APIClient client = ServiceGenerator.createService(APIClient.class);
                    Call<ResponseBody> result = client.getChangePassword(allaccess.getAuth_key(), etOldPass.getText().toString(), etNewPass.getText().toString());
                    result.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Response<ResponseBody> response) {
                            if (response.isSuccess()){
                                Log.e("ch", "ch3");
                                showDialog("Success", "Your password have been changed successfully.");
                            } else {
                                Log.e("ch", "ch4");
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });
                } else {
                    Log.e("ch", "ch5");
                }

            }
        });

    }

}

package com.example.reloadprepaid.fragment_activity.fragment_prepaidreload;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.adapter.HistoryAdapter;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.fragment_activity.fragment_prepaidreload.activity_fragmenthistory.ActivityFragmentHistoryList;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.WalletHistory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHistoryEWallet extends Fragment implements AdapterView.OnItemClickListener {

    ListView lvHistoryWallet;
    HistoryAdapter historyAdapter;
    AllAccess allAccess;
    ProgressDialog progressDialog;

    public FragmentHistoryEWallet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_history_ewallet, container, false);
        allAccess = (AllAccess) getActivity().getApplication();

        lvHistoryWallet = (ListView) view.findViewById(R.id.lvHistoryWallet);
        historyAdapter = new HistoryAdapter(getActivity());
        lvHistoryWallet.setAdapter(historyAdapter);
        lvHistoryWallet.setOnItemClickListener(this);

        readAPI();

        return view;
    }

    private void readAPI() {
        progressDialog = ProgressDialog.show(getActivity(), "Loading", "Please wait..", true);
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Call<ResponseBody> result = client.getHistory(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()) {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        JSONObject resultObj = jsonObject.getJSONObject("result");
                        JSONArray transactionArray = resultObj.getJSONArray("wallet_history");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        for (int i = 0; i < transactionArray.length(); i++) {
                            JSONObject transactionObj = transactionArray.getJSONObject(i);
                            Date datetime = sdf.parse(transactionObj.getString("datetime"));
                            boolean accountTopup = false;
                            if (transactionObj.getString("activity").equals("+")) {
                                accountTopup = true;
                            }

                            WalletHistory walletHistory = new WalletHistory(transactionObj.getString("ref_no"),
                                    transactionObj.getString("service"),
                                    transactionObj.getString("product"),
                                    transactionObj.getString("unit"),
                                    transactionObj.getString("amount"),
                                    transactionObj.getString("ref_account"),
                                    transactionObj.has("description") ? transactionObj.getString("description") : null,
                                    transactionObj.getString("status"),
                                    accountTopup,
                                    datetime);
                            historyAdapter.getWalletHistoryArrayList().add(walletHistory);
                        }
                        historyAdapter.notifyDataSetChanged();
                    }
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        WalletHistory walletHistory = historyAdapter.getWalletHistoryArrayList().get(position);
        String[] arrayLabel = {
                "Activity",
                "Status",
                "Service",
                "Reference No",
                "Reference Account",
                "Product",
                "Amount",
                "Payment Date",
                "Payment Time"
        };

        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyy");
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");

        String status;
        if (walletHistory.getStatus().equals("NEW")) {
            status = "New";
        } else if (walletHistory.getStatus().equals("PCS")) {
            status = "Processing";
        } else if (walletHistory.getStatus().equals("SNT")) {
            status = "Processed";
        } else if (walletHistory.getStatus().equals("SUC")) {
            status = "Success";
        } else {
            status = "Failed";
        }

        String[] arrayValue = {
                walletHistory.isAccountTopup() ? "Debit" : "Credit",
                status,
                walletHistory.getService(),
                walletHistory.getRefNo(),
                walletHistory.getRefAccount(),
                walletHistory.getDescription(),
                "RM" + walletHistory.getAmount(),
                sdfDate.format(walletHistory.getDateTime()),
                sdfTime.format(walletHistory.getDateTime()),
        };
        Intent intent = new Intent(getActivity(), ActivityFragmentHistoryList.class);
        intent.putExtra("arrayLabel", arrayLabel);
        intent.putExtra("arrayValue", arrayValue);
        startActivity(intent);
    }
}

package com.example.reloadprepaid.fragment_activity.activity_setting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.application.AllAccess;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityUpdateProfile extends AppCompatActivity {

    AllAccess allAccess;
    EditText etName, etMobile, etEmail;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_update_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Update Profile");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initializeComponents();
        readApi();
    }

    private void readApi() {
        progressDialog = ProgressDialog.show(ActivityUpdateProfile.this, "Loading", "Please wait..", true);
        APIClient client = ServiceGenerator.createService(APIClient.class);
        final Call<ResponseBody> result = client.getDashboard(allAccess.getAuth_key());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {

                try {
                    if (response.isSuccess()) {
                        Log.e("masuk", "lkasld");
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        etName.setText(jsonObject.getString("name"));
                        etMobile.setText(jsonObject.getString("mobile"));
                        etEmail.setText(jsonObject.getString("email"));
                        progressDialog.dismiss();
                    } else {
                        Log.e("masuk2", "lkasld");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();

                Log.e("masuk3", "lkasld");
            }
        });
    }

    private void initializeComponents() {
        allAccess = (AllAccess) getApplication();

        etName = (EditText) findViewById(R.id.etUpdateUsername);
        etMobile = (EditText) findViewById(R.id.etUpdateMobile);
        etEmail = (EditText) findViewById(R.id.etUpdateEmail);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickUpdateProfile(View view) {
        progressDialog = ProgressDialog.show(ActivityUpdateProfile.this, "Updating", "Please wait..");
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Log.e("NAMA", etName.getText().toString());
        Call<ResponseBody> result = client.getUpdate(allAccess.getAuth_key(), etName.getText().toString(), etMobile.getText().toString(), etEmail.getText().toString());
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                progressDialog.dismiss();
                AlertDialog alertDialog = new AlertDialog.Builder(ActivityUpdateProfile.this).create();
                alertDialog.setTitle("Success");
                alertDialog.setMessage("Your profile have been updated successfully.");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent("update_profile");
                        intent.putExtra("message", "TOLONGLA BOLEH!");
                        LocalBroadcastManager.getInstance(ActivityUpdateProfile.this).sendBroadcast(intent);
                        finish();
                    }
                });
                alertDialog.show();
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                showAlert("Error", "Please make sure you have a good internet connection and try again.");
            }
        });
    }

    public void showAlert(String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(ActivityUpdateProfile.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
    }

}

package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.modul.Product;

import java.util.List;

/**
 * Created by SyedSaufi on 17/02/2016.
 */
public class SpinnerProductAdapter extends ArrayAdapter<Product> {

    int prepaidPosition;
    List<Product> productList;
    Context context;

    public SpinnerProductAdapter(Context context, List<Product> objects, int prepaidPosition) {
        super(context, R.layout.row_spinnerprepaid_fragmentprepaidreload, objects);
        this.context = context;
        this.productList = objects;
        this.prepaidPosition = prepaidPosition;
        Log.e("CONSIZE: ", String.valueOf(productList.size()));

    }

    public List<Product> getProductList() {
        Log.e("GETSIZE: ", String.valueOf(productList.size()));
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_spinnerprepaid_fragmentprepaidreload, null);

        TextView tvSpinPrepaidName = (TextView) convertView.findViewById(R.id.tvSpinPrepaidName);

        tvSpinPrepaidName.setText(productList.get(position).getDescription());

        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_spinnerprepaid_fragmentprepaidreload, null);

        TextView tvSpinPrepaidName = (TextView) convertView.findViewById(R.id.tvSpinPrepaidName);
        TextView tvSelectPrepaid = (TextView) convertView.findViewById(R.id.tvSelectPrepaid);

        tvSelectPrepaid.setText("PRODUCT");
        tvSpinPrepaidName.setText(productList.get(position).getDescription());
        return convertView;
    }
}

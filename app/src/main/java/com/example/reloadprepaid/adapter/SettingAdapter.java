package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.reloadprepaid.R;

/**
 * Created by SyedSaufi on 24/02/2016.
 */
public class SettingAdapter extends BaseAdapter {

    Context context;
    String settings[] = {"Update Profile", "Change Password"};

    public SettingAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_fragmentsetting_lvsetting, null);

        ImageView ivSettings = (ImageView) convertView.findViewById(R.id.ivSettings);
        TextView tvSettingName = (TextView) convertView.findViewById(R.id.tvSettingName);

        if(position == 0){
            ivSettings.setImageResource(R.drawable.update_profile);
            tvSettingName.setText(settings[position]);
        } else if (position == 1){
            ivSettings.setImageResource(R.drawable.change_password);
            tvSettingName.setText(settings[position]);
        }
        return convertView;
    }
}

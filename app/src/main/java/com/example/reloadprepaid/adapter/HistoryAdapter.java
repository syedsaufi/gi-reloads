package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.modul.WalletHistory;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by SyedSaufi on 15/02/2016.
 */
public class HistoryAdapter extends BaseAdapter {

    ArrayList<WalletHistory> walletHistoryArrayList;
    Context context;

    public HistoryAdapter(Context context) {
        this.context = context;
        walletHistoryArrayList = new ArrayList<>();
    }

    public ArrayList<WalletHistory> getWalletHistoryArrayList() {
        return walletHistoryArrayList;
    }

    public void setWalletHistoryArrayList(ArrayList<WalletHistory> walletHistoryArrayList) {
        this.walletHistoryArrayList = walletHistoryArrayList;
    }

    @Override
    public int getCount() {
        return walletHistoryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return walletHistoryArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_fragmenthistory_ewallet, null);
        WalletHistory walletHistory = (WalletHistory) getItem(position);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        ImageView imgvStatus = (ImageView) convertView.findViewById(R.id.imgvStatus);
        TextView txtvService = (TextView) convertView.findViewById(R.id.txtvService);
        TextView txtvRefNo = (TextView) convertView.findViewById(R.id.txtvRefNo);
        TextView txtvAmount = (TextView) convertView.findViewById(R.id.txtvAmount);
        TextView txtvDate = (TextView) convertView.findViewById(R.id.txtvDate);

        txtvService.setText(walletHistory.getService());
        txtvRefNo.setText(": " + walletHistory.getRefNo());
        txtvAmount.setText("RM" + walletHistory.getAmount());
        txtvDate.setText(sdf.format(walletHistory.getDateTime()));

        if (walletHistory.getStatus().equals("NEW")) {
            Picasso.with(context).load(R.drawable.newprocess).into(imgvStatus);
        } else if (walletHistory.getStatus().equals("PCS") || walletHistory.getStatus().equals("SNT")) {
            Picasso.with(context).load(R.drawable.loading).into(imgvStatus);
        } else if (walletHistory.getStatus().equals("SUC")) {
            Picasso.with(context).load(R.drawable.success).into(imgvStatus);
        } else {
            Picasso.with(context).load(R.drawable.failed).into(imgvStatus);
        }

        return convertView;
    }
}

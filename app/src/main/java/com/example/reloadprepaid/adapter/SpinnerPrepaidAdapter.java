package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.modul.Prepaid;

import java.util.ArrayList;

public class SpinnerPrepaidAdapter extends ArrayAdapter<Prepaid> {

    Context context;
    ArrayList<Prepaid> prepaidList;
    Integer[] telcoList;


    public SpinnerPrepaidAdapter(Context context, ArrayList<Prepaid> objects) {
        super(context, R.layout.row_spinnerprepaid_fragmentprepaidreload, objects);
        this.context = context;
        if(objects != null){
            this.prepaidList = objects;
            telcoList = new Integer[]{R.drawable.telco_altel, R.drawable.telco_celcom, R.drawable.telco_digi, R.drawable.telco_maxis, R.drawable.telco_merchantrade, R.drawable.telco_tune_talk,
                    R.drawable.telco_u_mobile, R.drawable.telco_xox_mobile, R.drawable.telco_tm};
        }

    }

    public ArrayList<Prepaid> getPrepaidList() {
        return prepaidList;
    }

    public void setPrepaidList(ArrayList<Prepaid> prepaidArrayList) {
        this.prepaidList = prepaidArrayList;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_spinnerprepaidtelco_fragmentprepaidreload, null);

        TextView tvSpinPrepaidName = (TextView) convertView.findViewById(R.id.tvSpinPrepaidName);
        ImageView ivSpinnerTelco = (ImageView) convertView.findViewById(R.id.ivSpinnerTelco);

        ivSpinnerTelco.setImageResource(telcoList[position]);
        tvSpinPrepaidName.setText(prepaidList.get(position).getTecoCode());
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_spinnerprepaid_fragmentprepaidreload, null);

        TextView tvSpinPrepaidName = (TextView) convertView.findViewById(R.id.tvSpinPrepaidName);
        TextView tvSelectPrepaid = (TextView) convertView.findViewById(R.id.tvSelectPrepaid);

        tvSelectPrepaid.setText("OPERATOR");
        tvSpinPrepaidName.setText(prepaidList.get(position).getTecoCode());
        return convertView;
    }
}

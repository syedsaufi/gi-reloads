package com.example.reloadprepaid.adapter;

import android.accounts.Account;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.helper.APIClient;
import com.example.reloadprepaid.helper.ServiceGenerator;
import com.example.reloadprepaid.modul.AccountSummary;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SyedSaufi on 20/06/2016.
 */
public class SummaryAdapter extends BaseAdapter {

    String authKey;
    ArrayList<AccountSummary> accountArrayList;
    Context context;
    Boolean checkDealer;
    ProgressDialog progressDialog;

    public SummaryAdapter(Context context, String authKey) {
        this.context = context;
        this.authKey = authKey;
        accountArrayList = new ArrayList<>();
        checkDealer = false;
    }

    public ArrayList<AccountSummary> getAccountArrayList() {
        return accountArrayList;
    }

    public void setAccountArrayList(ArrayList<AccountSummary> accountArrayList) {
        this.accountArrayList = accountArrayList;
    }

    public Boolean getCheckDealer() {
        return checkDealer;
    }

    public void setCheckDealer(Boolean checkDealer) {
        this.checkDealer = checkDealer;
    }

    @Override
    public int getCount() {
        return accountArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return accountArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_fragmentsummary_lvaccount, null);

        TableRow tbrAccountName = (TableRow) convertView.findViewById(R.id.tbrAccountName);
        tbrAccountName.setVisibility(View.GONE);
        TextView tvAccType = (TextView) convertView.findViewById(R.id.tvAccType);
        TextView tvAccNo = (TextView) convertView.findViewById(R.id.tvAccNo);
        TextView tvAccName = (TextView) convertView.findViewById(R.id.tvAccName);
        TextView tvAccBal = (TextView) convertView.findViewById(R.id.tvAccBal);

        tvAccType.setText(accountArrayList.get(position).getAccountType());
        tvAccNo.setText(accountArrayList.get(position).getAccountNo());
        tvAccBal.setText(accountArrayList.get(position).getAccountBalance());

        FloatingActionButton btnTransferCredit = (FloatingActionButton) convertView.findViewById(R.id.fabCreditTransfer);
        btnTransferCredit.setVisibility(View.GONE);

        if (checkDealer) {
            tbrAccountName.setVisibility(View.VISIBLE);
            tvAccName.setText(accountArrayList.get(position).getAccountName());
            if (position == accountArrayList.size() - 1) {
                btnTransferCredit.setVisibility(View.VISIBLE);
                btnTransferCredit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View dialogView = inflater.inflate(R.layout.dialog_transferkredit, null);
                        final EditText etTransferUsername = (EditText) dialogView.findViewById(R.id.etTransferUsername);
                        final EditText etTransferAmount = (EditText) dialogView.findViewById(R.id.etTransferAmount);

                        final AlertDialog alertDialog = new AlertDialog.Builder(context)
                                .setView(dialogView)
                                .setTitle("Transfer Credit")
                                .setPositiveButton("Transfer", null)
                                .setNegativeButton("Cancel", null)
                                .create();

                        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                Button okButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                                okButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (etTransferUsername.getText().toString().equals("") || etTransferAmount.getText().toString().equals("")) {
                                            showAlert("Error", "Please make sure to fill in all the required fields before transfering.");
                                        } else {
                                            progressDialog = ProgressDialog.show(context, "Transfering credit", "Please wait...", true);
                                            APIClient client = ServiceGenerator.createService(APIClient.class);
                                            Call<ResponseBody> result = client.makeTransferCredit(authKey, etTransferUsername.getText().toString(), etTransferAmount.getText().toString());
                                            result.enqueue(new Callback<ResponseBody>() {
                                                @Override
                                                public void onResponse(Response<ResponseBody> response) {
                                                    if (response.isSuccess()) {
                                                        progressDialog.dismiss();
                                                        showAlert("Success", "Credit successfully transfered.");
                                                        alertDialog.dismiss();
                                                        Intent intent = new Intent("update_balance");
                                                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                                        Log.e("Success?", "YES");
                                                    } else {
                                                        progressDialog.dismiss();
                                                        showAlert("Error", "Credit fail to transfer, please try again.");
                                                        Log.e("Success?", "NO");
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Throwable t) {
                                                    progressDialog.dismiss();
                                                }
                                            });
                                        }
                                    }
                                });


                            }
                        });
                        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        alertDialog.show();

                    }
                });
            } else {
                btnTransferCredit.setVisibility(View.GONE);
            }
        } else {
            tbrAccountName.setVisibility(View.GONE);
        }

        return convertView;
    }

    public void showAlert(String title, String message) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();
    }

    public void displayMasterView(){

    }

    public void displayAgentView(){

    }

}

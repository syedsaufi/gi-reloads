package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.modul.Favourite;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by SyedSaufi on 16/02/2016.
 */
public class FavouriteAdapter extends BaseAdapter {

    ArrayList<Favourite> favouriteArrayList;
    Context context;

    public FavouriteAdapter(Context context) {
        this.context = context;
        favouriteArrayList = new ArrayList<>();
    }

    public ArrayList<Favourite> getFavouriteArrayList() {
        return favouriteArrayList;
    }

    public void setFavouriteArrayList(ArrayList<Favourite> favouriteArrayList) {
        this.favouriteArrayList = favouriteArrayList;
    }

    @Override
    public int getCount() {
        return favouriteArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_fragmentfavourite_lvfavourite, null);

        TextView tvFavouriteName = (TextView) convertView.findViewById(R.id.tvFavouriteName);
        TextView tvFavouritePhone = (TextView) convertView.findViewById(R.id.tvFavouritePhone);

        tvFavouriteName.setText(favouriteArrayList.get(position).getName());
        tvFavouritePhone.setText(favouriteArrayList.get(position).getPhone_no());

        return convertView;
    }
}

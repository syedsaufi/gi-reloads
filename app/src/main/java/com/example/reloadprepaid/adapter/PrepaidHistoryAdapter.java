package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.modul.PrepaidHistory;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by SyedSaufi on 24/02/2016.
 */
public class PrepaidHistoryAdapter extends BaseAdapter {

    Context context;
    ArrayList<PrepaidHistory> prepaidHistoryArrayList;

    public PrepaidHistoryAdapter(Context context) {
        this.context = context;
        prepaidHistoryArrayList = new ArrayList<>();
    }

    public ArrayList<PrepaidHistory> getPrepaidHistoryArrayList() {
        return prepaidHistoryArrayList;
    }

    public void setPrepaidHistoryArrayList(ArrayList<PrepaidHistory> prepaidHistoryArrayList) {
        this.prepaidHistoryArrayList = prepaidHistoryArrayList;
    }

    @Override
    public int getCount() {
        return prepaidHistoryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return prepaidHistoryArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_fragmenthistory_prepaidreload, null);
        PrepaidHistory prepaidHistory = (PrepaidHistory) getItem(position);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        ImageView imgvStatus = (ImageView) convertView.findViewById(R.id.imgvStatus);
        TextView txtvService = (TextView) convertView.findViewById(R.id.txtvService);
        TextView txtvReceipient = (TextView) convertView.findViewById(R.id.txtvReceipient);
        TextView txtvRefNo = (TextView) convertView.findViewById(R.id.txtvRefNo);
        TextView txtvAmount = (TextView) convertView.findViewById(R.id.txtvAmount);
        TextView txtvDate = (TextView) convertView.findViewById(R.id.txtvDate);

        txtvService.setText(prepaidHistory.getService());
        txtvReceipient.setText(": " + prepaidHistory.getRecipient());
        txtvRefNo.setText(": " + prepaidHistory.getRefNo());
        txtvAmount.setText("RM" + prepaidHistory.getAmount());
        txtvDate.setText(sdf.format(prepaidHistory.getDatetime()));

        if (prepaidHistory.getStatus().equals("NEW")) {
            Picasso.with(context).load(R.drawable.newprocess).into(imgvStatus);
        } else if (prepaidHistory.getStatus().equals("PCS") || prepaidHistory.getStatus().equals("SNT")) {
            Picasso.with(context).load(R.drawable.loading).into(imgvStatus);
        } else if (prepaidHistory.getStatus().equals("SUC")) {
            Picasso.with(context).load(R.drawable.success).into(imgvStatus);
        } else {
            Picasso.with(context).load(R.drawable.failed).into(imgvStatus);
        }

        return convertView;
    }
}

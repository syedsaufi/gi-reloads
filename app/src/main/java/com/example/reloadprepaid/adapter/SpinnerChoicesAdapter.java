package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.reloadprepaid.R;

/**
 * Created by SyedSaufi on 18/02/2016.
 */
public class SpinnerChoicesAdapter extends ArrayAdapter<String> {

    Context context;

    public SpinnerChoicesAdapter(Context context, String choiceArray[]) {
        super(context, R.layout.row_spinnerprepaid_fragmentprepaidreload, choiceArray);
        this.context = context;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_spinnerprepaid_fragmentprepaidreload, null);

        TextView tvChoices = (TextView) convertView.findViewById(R.id.tvSpinPrepaidName);
        tvChoices.setText(getItem(position));
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_spinnerprepaid_fragmentprepaidreload, null);

        TextView tvChoices = (TextView) convertView.findViewById(R.id.tvSpinPrepaidName);
        TextView tvPilih = (TextView) convertView.findViewById(R.id.tvSelectPrepaid);

        tvPilih.setText("Choice");
        tvChoices.setText(getItem(position));
        return convertView;
    }
}

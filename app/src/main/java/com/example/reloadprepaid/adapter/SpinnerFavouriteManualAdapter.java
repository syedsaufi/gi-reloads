package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.modul.Favourite;

import java.util.List;

/**
 * Created by SyedSaufi on 18/02/2016.
 */
public class SpinnerFavouriteManualAdapter extends ArrayAdapter<Favourite> {

    Context context;
    List<Favourite> favouriteList;

    public SpinnerFavouriteManualAdapter(Context context, List<Favourite> object) {
        super(context, R.layout.row_spinnerprepaid_fragmentprepaidreload, object);
        this.context = context;
        this.favouriteList = object;

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_spinnerprepaid_fragmentprepaidreload, null);

        TextView tvChoices = (TextView) convertView.findViewById(R.id.tvSpinPrepaidName);
        tvChoices.setText(getItem(position).getName());
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_spinnerprepaid_fragmentprepaidreload, null);

        TextView tvChoices = (TextView) convertView.findViewById(R.id.tvSpinPrepaidName);
        TextView tvPilih = (TextView) convertView.findViewById(R.id.tvSelectPrepaid);

        tvPilih.setText("Favourite");
        tvChoices.setText(getItem(position).getName());
        return convertView;
    }
}

package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.reloadprepaid.R;
import com.example.reloadprepaid.modul.ResellerOperator;
import com.example.reloadprepaid.modul.ResellerProduct;

import java.util.ArrayList;

/**
 * Created by SyedSaufi on 10/02/2016.
 */
public class PrepaidAdapter extends BaseExpandableListAdapter {
    Context context;
    ArrayList<ResellerOperator> resellerOperatorArrayList;

    public PrepaidAdapter(Context context) {
        this.context = context;
        resellerOperatorArrayList = new ArrayList<>();
    }

    public ArrayList<ResellerOperator> getResellerOperatorArrayList() {
        return resellerOperatorArrayList;
    }

    public void setResellerOperatorArrayList(ArrayList<ResellerOperator> resellerOperatorArrayList) {
        this.resellerOperatorArrayList = resellerOperatorArrayList;
    }

    @Override
    public int getGroupCount() {
        return resellerOperatorArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return resellerOperatorArrayList.get(groupPosition).getProductArrayList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return resellerOperatorArrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return resellerOperatorArrayList.get(groupPosition).getProductArrayList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_fragmentretellerprice_xlvprepaidtitle, null);
        ResellerOperator resellerOperator = (ResellerOperator) getGroup(groupPosition);

        TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        tvTitle.setText(resellerOperator.getOperatorName());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_fragmentretellerprice_xlvprepaidcontent, null);
        ResellerProduct resellerProduct = (ResellerProduct) getChild(groupPosition, childPosition);

        TextView txtvDescription = (TextView) convertView.findViewById(R.id.txtvDescription);
        TextView txtvPrice = (TextView) convertView.findViewById(R.id.txtvPrice);
        TextView txtvSaving = (TextView) convertView.findViewById(R.id.txtvSaving);

        txtvDescription.setText(resellerProduct.getDescription());
        txtvPrice.setText("RM" + resellerProduct.getPrice());
        txtvSaving.setText("RM" + resellerProduct.getSaving());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}

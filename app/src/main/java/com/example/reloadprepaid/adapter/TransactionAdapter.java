package com.example.reloadprepaid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.reloadprepaid.R;

/**
 * Created by SyedSaufi on 23/02/2016.
 */
public class TransactionAdapter extends BaseAdapter {

    Context context;
    String[] arrayLabel;
    String[] arrayValue;

    public TransactionAdapter(Context context, String[] arrayLabel, String[] arrayValue) {
        this.context = context;
        this.arrayLabel = arrayLabel;
        this.arrayValue = arrayValue;
    }

    @Override
    public int getCount() {
        return arrayLabel.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.row_fragmenthistory_activityfragmenthistory_lvhistorydetails, null);

        TextView txtvLabel = (TextView) convertView.findViewById(R.id.txtvLabel);
        TextView txtvValue = (TextView) convertView.findViewById(R.id.txtvValue);
        txtvLabel.setText(arrayLabel[position]);
        txtvValue.setText(arrayValue[position]);

        return convertView;
    }
}

package com.example.reloadprepaid.application;

import android.app.Application;

import com.example.reloadprepaid.modul.Favourite;
import com.example.reloadprepaid.modul.WalletHistory;

/**
 * Created by SyedSaufi on 10/02/2016.
 */
public class AllAccess extends Application {

    public String auth_key = "";

    public String getAuth_key() {
        return auth_key;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public String operator, product, product_id;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}

package com.example.reloadprepaid.helper;

import android.text.Editable;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by SyedSaufi on 10/02/2016.
 */
public interface APIClient {

    //member
    //register
    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @POST("member/")
    Call<ResponseBody> getRegister(@Field("mode") String email,
                                   @Field("data") String data);

    //login
    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @POST("member/login")
    Call<ResponseBody> getLogin(@Field("username") String username,
                                @Field("password") String password);

    //dashboard, guna dalam page home dan dashboard
    @Headers("api_key: GIRELOAD2016")
    @GET("member/dashboard")
    Call<ResponseBody> getDashboard(@Header("auth_key") String auth_key);

    //update profile
    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @POST("member/updateprofile")
    Call<ResponseBody> getUpdate(@Header("auth_key") String auth_key,
                                 @Field("name") String name,
                                 @Field("mobile") String mobile,
                                 @Field("email") String email);

    //change password
    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @POST("member/changepassword")
    Call<ResponseBody> getChangePassword(@Header("auth_key") String auth_key,
                                         @Field("old_password") String odlPassword,
                                         @Field("new_password") String newPassword);

    //forgot password
    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @POST("member/resetpassword")
    Call<ResponseBody> getForgpotPassword(@Field("email") String email);

    //---------------------------------------------------------------------------//

    //member
    //favourite number
    //add favourite number
    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @POST("member/favnumber")
    Call<ResponseBody> addFavourite(@Header("auth_key") String authKey,
                                    @Field("name") String faveName,
                                    @Field("mobile") String faveMobile);

    //get favourite number
    @Headers("api_key: GIRELOAD2016")
    @GET("member/favnumber")
    Call<ResponseBody> getFavourite(@Header("auth_key") String auth_key);

    //edit favourite number
    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @PUT("member/favnumber/{id}")
    Call<ResponseBody> editFavourite(@Path("id") String id,
                                     @Header("auth_key") String authKey,
                                     @Field("name") String faveName,
                                     @Field("mobile") String faveMobile);

    //delete favourite number
    @Headers("api_key: GIRELOAD2016")
    @DELETE("member/favnumber/{id}")
    Call<ResponseBody> deleteFavourite(@Path("id") String id,
                                       @Header("auth_key") String authKey);

    //---------------------------------------------------------------------------//

    //transaction
    @Headers("api_key: GIRELOAD2016")
    @GET("prepaid/resellerprice")
    Call<ResponseBody> getResellerPrice(@Header("auth_key") String auth_key);

    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @POST("prepaid/transaction")
    Call<ResponseBody> makeTransaction(@Header("auth_key") String auth_key,
                                       @Field("product_id") String productId,
                                       @Field("phone_no") String phoneNumber);

    @Headers("api_key: GIRELOAD2016")
    @GET("prepaid/history")
    Call<ResponseBody> getHistory(@Header("auth_key") String auth_key);

    //---------------------------------------------------------------------------//

    //wallet
    //transfer credit in dashboard
    @Headers("api_key: GIRELOAD2016")
    @FormUrlEncoded
    @POST("wallet/transfercredit")
    Call<ResponseBody> makeTransferCredit(@Header("auth_key") String auth_key,
                                          @Field("username") String username,
                                          @Field("amount") String amount);


}
